/**
 * I don't recommend you go down further unless you want to dwell down into a mess of for loops, statements and
 * general spaghetti to get this to work.
 * 
 * But hey! The more power to ya!
 * 
 * - Øyvind
 */

// Check for the various File API support.
if (window.File && window.FileReader && window.FileList && window.Blob) {
    // Great success! All the File APIs are supported.
} else {
    alert('The File APIs are not fully supported in this browser.');
}

window.onload = function () {
    document.getElementById('files').addEventListener('change', handleFileSelect, false);

    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object
            f = files[0];
            var reader = new FileReader();
    
            // Closure to capture the file information.
            reader.onload = (function() {
                return function(e) {
                    JsonObj = JSON.parse(e.target.result);
                    console.log(JsonObj);
                    updateQuestions(JsonObj);
                };
            })(f);
        reader.readAsText(f);
    }
}

function checkAnswers(questionGroups, explanationGroups)
{
    // Counting the questions we are passing by during our checks.
    // This ensures that if questions have a variable amount of questions, that we won't get an out-of-bounds array exception.
    var questionsPassed = 0;
    // Iterating through every question in the subchapter
    for (var i = 0; i < questionGroups.length; i++) {
        var question = questionGroups[i].getElementsByTagName("input");

        // Checking all the options within one of the question groups...
        for (var n = 0; n < question.length; n++) {
            // ... And if we found the answer the user picked...
            if (question[n].checked) {
                // Display the correct/incorrect values from the json file.
                var explanation = explanationGroups[questionsPassed];
                explanation.style.display = 'inline'

                // ... We can check for the correct answer here!
                // TODO: Make a score value to increase here for every correct question!
            }
            // We moved past a question, adding it to the count.
            questionsPassed++;
        }
    }
}

// Create a basic container with a section to keep it all inside with a title / heading with your own text.
// containerElement = The container to put it all inside (div, article, section)
// titleElement = The type of heading you want (h1/h2/p)
// titleText = The text to put as your heading
// Return: The newly created container.
function createContainer(containerElement, titleElement, titleText, onClickHide) {
    var container = document.createElement(containerElement);
    var title = document.createElement(titleElement);
    var titleText = document.createTextNode(titleText);
    title.appendChild(titleText);
    container.appendChild(title);

    if (onClickHide) {
        createOnClickHider(title);
    }

    return container;
}

function createInputSelection(inputType, inputName, inputValue) {
    var input = document.createElement('input');
    input.type = inputType;
    input.name = inputName;
    input.value = inputValue;
    return input;
}

function createSubmitButton(questions, explanations) {
    var submitButton = document.createElement('button');
    submitButton.onclick = function() {
        checkAnswers(questions, explanations);
    }
    submitButton.textContent = "Submit";
    submitButton.style.display = 'none';

    return submitButton;
}

function createAnswerSelector(isMultipleChoice, questionNumber, answerNumber, answer) {
    // Create the label to show what the alternative is.
    var label = document.createElement('label');

    // Check if our question is multiple choice or not.
    if (isMultipleChoice) {
        // Create a checkbox to select multiple options.
        label.appendChild(createInputSelection('checkbox', 'q' + questionNumber, String.fromCharCode(65 + answerNumber)));
    }
    else {
        // Create the radio button to allow the user to select their answer.
        label.appendChild(createInputSelection('radio', 'q' + questionNumber, String.fromCharCode(65 + answerNumber)));
    }
    // Adding the button and text to the label
    label.appendChild(document.createTextNode(answer));
    label.appendChild(document.createElement("br"));

    return label;
}

function createParagraph(paragraphText) {
    var paragraph = document.createElement('p');
    paragraph.appendChild(document.createTextNode(paragraphText));

    return paragraph;
}

function createOnClickHider(container) {
    container.onclick = function () {
        // Prevents the parent from triggering if we're clicking on the child.
        event.stopPropagation();

        // Skipping first element as this is the title object.
        var parent = container.parentNode;
        var divs = parent.childNodes;
        for (var i = 1; i < divs.length; i++) {
            if (divs[i].style.display === 'inline') {
                divs[i].style.display = 'none';
            } else {
                divs[i].style.display = 'inline';
            }
        }
    }
}

function createBreaklineWithStyle() {
    var breakLine = document.createElement("br");
    breakLine.style.display = 'none';

    return breakLine;
}

function updateQuestions(json) {
    var output = document.getElementById('output');
    var chapters = json.chapters;

    // Saving reference to the chapter and subchapter arrays to handle "onclick" functions later.
    var chapterArray = [];
    var subchapterArray = [];

    // Iterating through every chapter...
    for (var chapter = 0; chapter < chapters.length; chapter++) {
        // Gathering all of the subchapters contained in our current chapter.
        var subchapters = chapters[chapter].subchapters;

        // var chapterContainer = createContainer('section', 'h1', `Chapter ${chapter + 1}`, true);
        var chapterContainer = createContainer('section', 'h1', chapters[chapter].chapterName, true);
        chapterContainer.childNodes[0].id = 'ChapterText';
        chapterContainer.style.display = 'inline';

        output.appendChild(chapterContainer);

        // ...while iterating through every sub-chapter...
        for (var subchapter = 0; subchapter < subchapters.length; subchapter++) {
            // Gathering all of the questions in our current subchapter
            var questions = subchapters[subchapter].questions;

            // var subchapterContainer = createContainer('section', 'h2', `Subchapter ${subchapter + 1}`, true);
            var subchapterContainer = createContainer('section', 'h2', subchapters[subchapter].subchapterName, true);
            subchapterContainer.childNodes[0].id = 'SubChapterText';
            subchapterContainer.style.display = 'none';

            chapterContainer.appendChild(subchapterContainer);

            // Storing a reference to every question per subchapter.
            var questionArray = [];
            var explanationArray = [];

            // ... and every question.
            for (var question = 0; question < questions.length; question++) {
                var currentQuestion = questions[question];

                // Create the question line on the top
                var questionContainer = createContainer('section', 'p', currentQuestion.question, false);
                questionContainer.style.display = 'none';

                subchapterContainer.appendChild(questionContainer);

                // If a question has demonstration code, we create a code section below the question.
                var demoCode = currentQuestion.code;
                if (demoCode) {
                    var codeContainer = document.createElement('pre'); // Ensures we keep the "tabs" from the json file
                    codeContainer.appendChild(document.createElement('code')); // Formats it as code. Just looks pretty.

                    // Write down line for line the code spesified in the json file.
                    for (var i = 0; i < demoCode.length; i++) {
                        var questionCodeText = document.createTextNode(demoCode[i]); // Make line
                        codeContainer.appendChild(questionCodeText); // Add line to the code block

                        if (i != demoCode.length - 1) {
                            // Don't break-line if we're on the last line, for formating reasons.
                            codeContainer.appendChild(document.createElement("br")); // New line
                        }
                    }
                    questionContainer.appendChild(codeContainer);
                }

                // Before adding all of the answer alternatives below that again.
                for (var answer = 0; answer < currentQuestion.answers.length; answer++) {
                    var label = createAnswerSelector(currentQuestion.multipleChoice, question, answer, currentQuestion.answers[answer]);

                    // Adding the answer to the questionContainer to display it.
                    questionContainer.appendChild(label);

                    // Adding the explanation to the question.
                    var explanationParagraph = createParagraph(currentQuestion.explanations[answer]);
                    explanationParagraph.appendChild(document.createElement('br'));

                    // Sorting out correct and incorrect answers for formatting in CSS.
                    if (currentQuestion.correctAnswer === String.fromCharCode(65 + answer)) {
                        explanationParagraph.id = 'CorrectAnswerText'
                    } else {
                        explanationParagraph.id = 'IncorrectAnswerText'
                    }

                    // Invisible until the users answers the question
                    explanationParagraph.style.display = 'none';

                    // Adding the element to the question container to be below the different answers.
                    explanationArray.push(questionContainer.appendChild(explanationParagraph));
                }
                // Storing the section for later use.
                questionArray.push(questionContainer);
            }
            // Creating the submit button for each subchapter.
            subchapterContainer.appendChild(createBreaklineWithStyle());
            subchapterContainer.appendChild(createSubmitButton(questionArray, explanationArray));

            // Storing the section for later use.
            subchapterArray.push(subchapterContainer);
        }
        // Storing the section for later use.
        chapterArray.push(chapterContainer);
    }
}